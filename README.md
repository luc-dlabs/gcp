# Repo with some hints/guides for successful training and passing GCP certification

[Documentation worth reading before certifying](./common.md) - choose topics based on exam guide appropriate for your
certification

[Google Associate Cloud Engineer Certification](./engineer.md)  
[Google Professional Cloud Architect Certification](./architect.md)  
[Professional Machine Learning Engineer Certification](./machinelearning.md)  

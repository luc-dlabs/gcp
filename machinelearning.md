# Professional Machine Learning Engineer Certification Guide
## Get Trained
- Review the exam guide to understand the scope of the certification exam and technical areas to focus  (estimated
  time: 15-30 min) - https://cloud.google.com/certification/guides/machine-learning-engineer
- Take the practice exam - https://cloud.google.com/certification/sample-questions/machine-learning-engineer
- Review the Certification FAQ (estimated time: 15-30 min) - https://cloud.google.com/certification/faqs/
- Complete Machine Learning trainings through Coursera (fundamentals first, then __Machine Learning with TensorFlow on 
  Google Cloud Platform Specialization__ and __Advanced Machine Learning on Google Cloud Specialization__ tracks):  
  1.1 [Google Cloud Platform Big Data and Machine Learning Fundamentals](https://www.coursera.org/learn/gcp-big-data-ml-fundamentals)  
  2.1 [How Google does Machine Learning](https://www.coursera.org/learn/google-machine-learning)  
  2.2 [Launching into Machine Learning](https://www.coursera.org/learn/launching-machine-learning)  
  2.3 [Introduction to TensorFlow](https://www.coursera.org/learn/intro-tensorflow)  
  2.4 [Feature Engineering](https://www.coursera.org/learn/feature-engineering)  
  2.5 [Art and Science of Machine Learning](https://www.coursera.org/learn/art-science-ml)  
  3.1 [End-to-End Machine Learning with TensorFlow on GCP](https://www.coursera.org/learn/end-to-end-ml-tensorflow-gcp)  
  3.2 [Production Machine Learning Systems](https://www.coursera.org/learn/gcp-production-ml-systems)  
  3.3 [Image Understanding with TensorFlow on GCP](https://www.coursera.org/learn/image-understanding-tensorflow-gcp)  
  3.4 [Sequence Models for Time Series and Natural Language Processing](https://www.coursera.org/learn/sequence-models-tensorflow-gcp)  
  3.5 [Recommendation Systems with TensorFlow on GCP](https://www.coursera.org/learn/recommendation-models-gcp)  
  
     
## Hands-On Labs (Qwiklabs)
Complete the following 2 mandatory quests:  
 1. [Scientific Data Processing](https://www.qwiklabs.com/quests/28) (6 labs) (estimated time: 6 hours)  
 2. [Data Science on Google Cloud](https://www.qwiklabs.com/quests/43) (10 labs) (estimated time: 1 day)  

Optional:  
 1. [Data Science on Google Cloud: Machine Learning](https://www.qwiklabs.com/quests/50) (5 labs) (estimated time: 7 
    hours)  

Practice and develop your coding skills with [Google Developer Codelabs](https://codelabs.developers.google.com/?cat=cloud) 
(optional) (estimated time: 4-8 hours)  


# Documentation worth reading before certifying
Review the [cloud infrastructure solutions](https://cloud.google.com/solutions/) at Google Cloud Solutions under the 
following categories of compute, storage, networking, etc. (estimated time: 10-12 hours) 


## Compute  
 - [Using Clusters for Large-scale Technical Computing in the Cloud](https://cloud.google.com/solutions/using-clusters-for-large-scale-technical-computing)
 - [Designing Robust Systems](https://cloud.google.com/compute/docs/tutorials/robustsystems)
 - [Image Management Best Practices](https://cloud.google.com/compute/docs/images/image-management-best-practices)
 - [Deploying MongoDB on Google Compute Engine](https://cloud.google.com/community/tutorials/mongodb-atlas-appengineflex-nodejs-app)
 - Using Firebase for Real-time Events on App Engine // dead link  
 - [Setting Up LAMP on Compute Engine](https://cloud.google.com/community/tutorials/setting-up-lamp)
 - [Running Windows Server Failover Clustering](https://cloud.google.com/compute/docs/tutorials/running-windows-server-failover-clustering)
 - [Choosing a Computing Option](https://cloud.google.com/hosting-options)
 - [Best Practices for Compute Engine Region Selection](https://cloud.google.com/solutions/best-practices-compute-engine-region-selection)
 - [Reliable task scheduling on Compute Engine with Cloud Scheduler](https://cloud.google.com/solutions/reliable-task-scheduling-compute-engine)
 - [Deploying a Multi-Subnet SQL Server 2016 Always On Availability Group on Compute Engine](https://cloud.google.com/solutions/deploy-multi-subnet-sql-server)

## Storage  
 - [Transferring Big Data Sets to Cloud Platform](https://cloud.google.com/solutions/migration-to-google-cloud-transferring-your-large-datasets)
 - [Automating the Classification of Data Uploaded to Cloud Storage](https://cloud.google.com/solutions/automating-classification-of-data-uploaded-to-cloud-storage)
 - [Building Scalable Web Applications with Cloud Datastore](https://cloud.google.com/solutions/building-scalable-web-apps-with-cloud-datastore)
 - [Choosing the Right Architecture for Global Data Distribution](https://cloud.google.com/solutions/architecture/global-data-distribution)
 - [Loading, Storing, and Archiving Time Series Data](https://cloud.google.com/docs/tutorials#%22time+series%22)
 - [Choosing a Storage Option](https://cloud.google.com/products/storage)

## Networking  
 - [Application Capacity Optimizations with Global Load Balancing](https://cloud.google.com/solutions/about-capacity-optimization-with-global-lb)
 - [Best Practices for Floating IP Addresses](https://cloud.google.com/solutions/best-practices-floating-ip-addresses)
 - [Building High-throughput VPNs](https://cloud.google.com/solutions/building-high-throughput-vpns)
 - [Build high availability and high bandwidth NAT gateways](https://cloud.google.com/vpc/docs/special-configurations#multiple-natgateways)
 - [Using APIs from an External Network](https://cloud.google.com/vpc/docs/configure-private-google-access-hybrid)
 - [Automated Network Deployment: Building a VPN Between GCP and AWS](https://cloud.google.com/solutions/automated-network-deployment-multicloud)
 - [How to set up remote access to MySQL on Compute Engine](https://cloud.google.com/solutions/mysql-remote-access)
 - [Exposing gRPC services as REST APIs using Cloud Endpoints](https://cloud.google.com/endpoints/docs/grpc/about-grpc)

## Security & IAM  
 - [Best Practices for DDoS Protection and Mitigation on Google Cloud Platform](https://cloud.google.com/files/GCPDDoSprotection-04122016.pdf)
 - [Securing your Cloud Platform Account with Security Keys](https://support.google.com/accounts/answer/6103523)
 - [Scenarios for Exporting Stackdriver Logging: Security and Access Analytics](https://cloud.google.com/solutions/exporting-stackdriver-logging-for-security-and-access-analytics)
 - [Policy Design for Customers](https://cloud.google.com/solutions/policies/designing-gcp-policies)
 - [Securing Rendering Workloads](https://cloud.google.com/solutions/securing-rendering-workloads)
 - [Authentication in HTTP Cloud Functions](https://cloud.google.com/functions/docs/writing/http)
 - [Securely Connecting to VM Instances](https://cloud.google.com/solutions/connecting-securely)
 - [Deploying a Fault-Tolerant Microsoft Active Directory Environment](https://cloud.google.com/solutions/deploy-fault-tolerant-active-directory-environment)
 - [Federating Google Cloud Platform with Active Directory: Configuring single sign-on](https://cloud.google.com/architecture/identity/federating-gcp-with-active-directory-configuring-single-sign-on)

## Deployment  
 - [Best Practices for Using Deployment Manager](https://cloud.google.com/deployment-manager/docs/best-practices/)
 - [Automated Network Deployment: Startup](https://cloud.google.com/solutions/automated-network-deployment-startup)
 - [Creating a Shared VPC with Deployment Manager](https://cloud.google.com/solutions/shared-vpc-with-deployment-manager)
 - [Compute Engine Management with Puppet, Chef, Salt, and Ansible](https://cloud.google.com/docs/tutorials#devops)

## Logging, Monitoring  
 - [Design Patterns for Exporting Stackdriver Logging](https://cloud.google.com/solutions/design-patterns-for-exporting-stackdriver-logging)
 - [Customizing Stackdriver Logs for Kubernetes Engine with Fluentd](https://cloud.google.com/solutions/customizing-stackdriver-logs-fluentd)
 - [Autoscaling an Instance Group with Stackdriver Custom Metrics](https://cloud.google.com/solutions/autoscaling-instance-group-with-custom-stackdrivers-metric)
 - [Using Stackdriver Uptime Checks for Triggering Cloud Functions on a Schedule](https://cloud.google.com/scheduler/docs/tut-pub-sub)

## CI/CD, Development & Test  
 - [Continuous Deployment to Kubernetes Engine using Jenkins](https://cloud.google.com/solutions/continuous-delivery-jenkins-kubernetes-engine)
 - Continuous Deployment on Compute Engine Using Ansible with Spinnaker // deadlink :-(
 - [Continuous Delivery Pipelines with Spinnaker and Kubernetes Engine](https://cloud.google.com/solutions/continuous-delivery-spinnaker-kubernetes-engine)
 - [Automating Canary Analysis on Google Kubernetes Engine with Spinnaker](https://cloud.google.com/solutions/automated-canary-analysis-kubernetes-engine-spinnaker)
 - [Using Jenkins for Distributed Builds on Compute Engine](https://cloud.google.com/solutions/using-jenkins-for-distributed-builds-on-compute-engine)

## Microservices & Containers
 - [Preparing a Kubernetes Engine Environment for Production](https://cloud.google.com/solutions/prep-kubernetes-engine-for-prod)
 - [Heterogeneous Deployment Patterns with Kubernetes](https://cloud.google.com/solutions/heterogeneous-deployment-patterns-with-kubernetes)
 - [Best Practices for Building Containers](https://cloud.google.com/solutions/best-practices-for-building-containers)
 - [Best Practices for Operating Containers](https://cloud.google.com/solutions/best-practices-for-operating-containers)
 - [Deploying Memcached on Kubernetes Engine](https://cloud.google.com/solutions/deploying-memcached-on-kubernetes-engine)
 - [Architecture: Scalable Commerce Workloads using Microservices](https://cloud.google.com/solutions/architecture/scaling-commerce-workloads-architecture)
 - [Using Google Cloud Platform Services from Google Kubernetes Engine](https://cloud.google.com/solutions/gaming/running-dedicated-game-servers-in-kubernetes-engine)
 - [Creating GKE Private Clusters with Network Proxies for External Access](https://cloud.google.com/solutions/creating-kubernetes-engine-private-clusters-with-net-proxies)
 - [Running Dedicated Game Servers in Kubernetes Engine](https://cloud.google.com/solutions/gaming/running-dedicated-game-servers-in-kubernetes-engine)
 - [Distributed Load Testing Using Kubernetes](https://cloud.google.com/solutions/distributed-load-testing-using-kubernetes)
 - [Choose Size and Scope of Google Kubernetes Engine Clusters](https://cloud.google.com/kubernetes-engine/docs/how-to/cluster-admin-overview)
 - [Using Kubernetes Engine to Deploy Apps with Regional Persistent Disks](https://cloud.google.com/solutions/using-kubernetes-engine-to-deploy-apps-with-regional-persistent-disks)
 - [Help secure software supply chains on Google Kubernetes Engine](https://cloud.google.com/solutions/secure-software-supply-chains-on-google-kubernetes-engine)

## Mobile Apps  
 - [Mobile App Backend Services](https://firebase.google.com/docs/cloud-messaging/fcm-architecture)
 - [Build a Mobile App Using Google Compute Engine and REST](https://cloud.google.com/solutions/mobile/mobile-compute-engine-rest)
 - [Build an Android App Using Firebase and the App Engine Flexible Environment](https://cloud.google.com/solutions/mobile/mobile-firebase-app-engine-flexible)

## Open Source  
 - [Google Cloud Platform for OpenStack Users](https://cloud.google.com/docs/compare/openstack/)

## Migration  
 - [Best Practices for Migrating Virtual Machines to Compute Engine](https://cloud.google.com/solutions/migrating-vms-migrate-for-compute-engine-getting-started)
 - [Best Practices for App Engine Standard Environment Memcache](https://cloud.google.com/appengine/docs/standard/python/migrate-to-python3/memcache-to-memorystore)
 - [Migrating On-Premises Hadoop Infrastructure to Google Cloud Platform](https://cloud.google.com/solutions/migration/hadoop/hadoop-gcp-migration-overview)
 - [Migrating HDFS Data from On-Premises to Google Cloud Platform](https://cloud.google.com/solutions/migration/hadoop/hadoop-gcp-migration-data)
 - [Migrating a MySQL Cluster to Compute Engine Using HAProxy](https://cloud.google.com/solutions/migrating-mysql-cluster-compute-engine-haproxy)
 - [Architecture: Delivering Aggregated Travel Data with Minimal Latency](https://cloud.google.com/solutions/delivering-aggregated-travel-data-with-minimal-latency)
 - [Migrating from DynamoDB to Cloud Spanner](https://cloud.google.com/solutions/migrating-dynamodb-to-cloud-spanner)

## Hybrid Cloud  
 - [Hybrid and Multi-Cloud Patterns and Practices](https://cloud.google.com/solutions/hybrid-and-multi-cloud-patterns-and-practices)
 - [Hybrid and Multi-Cloud Architecture Patterns](https://cloud.google.com/solutions/hybrid-and-multi-cloud-architecture-patterns)
 - [Hybrid and Multi-Cloud Network Topologies](https://cloud.google.com/solutions/hybrid-and-multi-cloud-network-topologies)
 - Hybrid Connectivity Using Your Own Public IP Addresses on Compute Engine // dead link
 - [Building a Hybrid Render Farm](https://cloud.google.com/solutions/building-a-hybrid-render-farm)
 - Deploying the Elastifile Cross-Cloud Data Fabric // dead link

## Backup, Archival and Disaster Recovery  
 - [Disaster Recovery Building Blocks](https://cloud.google.com/solutions/dr-scenarios-building-blocks)
 - [Disaster Recovery Scenarios for Applications](https://cloud.google.com/solutions/dr-scenarios-for-applications)
 - [Disaster Recovery Scenarios for Data](https://cloud.google.com/solutions/dr-scenarios-for-data)
 - [Building a Microsoft SQL Server Disaster Recovery Plan with Compute Engine](https://cloud.google.com/solutions/dr-scenarios-building-blocks)

## SAP on GCP  
 - [Architecture: SAP Hybris Deployment](https://cloud.google.com/solutions/sap/docs/sap-hybris-deployment-architecture)
 - [Running SAP Hybris Using SAP HANA](https://cloud.google.com/solutions/sap/docs/sap-hybris-on-sap-hana)
 - [SAP HANA High Availability and Disaster Recovery Planning](https://cloud.google.com/solutions/sap/docs/sap-hana-ha-planning-guide)
 - [Integrating GCP services with Cloud Foundry on SAP Cloud Platform](https://cloud.google.com/solutions/partners/sap/scp/scp-cf-gcp-service-broker-guide)

Review the [sample architecture flow charts](https://cloud.google.com/solutions/)
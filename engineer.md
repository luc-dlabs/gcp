# Google Associate Cloud Engineer Certification Guide
## Get Trained
- Review the exam guide to understand the scope of the certification exam and technical areas to focus  (estimated
  time: 15-30 min) - https://cloud.google.com/certification/guides/cloud-engineer/  
- Take the practice exam (estimated time: 1-2 hours) - https://cloud.google.com/certification/practice-exam/cloud-engineer  
- Review the Certification FAQ (estimated time: 15-30 min) - https://cloud.google.com/certification/faqs/  
- Complete the “Cloud Infrastructure” track trainings through Coursera. Complete the first 5 courses of Architecting 
  with Google Cloud Platform Specialization (listed below):  
  1. [Google Cloud Platform Fundamentals: Core Infrastructure](https://www.coursera.org/learn/gcp-fundamentals)
     (estimated time: 11 hours)  
  2. [Essential Cloud Infrastructure: Foundation](https://www.coursera.org/learn/gcp-infrastructure-foundation)
     (estimated time: 9 hours)  
  3. [Essential Cloud Infrastructure: Core Services](https://www.coursera.org/learn/gcp-infrastructure-core-services) 
     (estimated time: 11 hours)  
  4. [Elastic Cloud Infrastructure: Scaling and Automation](https://www.coursera.org/learn/gcp-infrastructure-scaling-automation)
     (estimated time: 11 hours)  
  5. [Reliable Google Cloud Infrastructure: Design and Process](https://www.coursera.org/learn/cloud-infrastructure-design-process)
     (estimated time: 9 hours)  
     
For those with an AWS background, review the [Google Cloud Platform for AWS Professionals](https://cloud.google.com/docs/compare/aws/).
Similarly review the [Google Cloud Platform for Azure Professionals](https://cloud.google.com/docs/compare/azure/) for 
those familiarity with Azure (estimated time: 2-3 hours)  


## Hands-On Labs (Qwiklabs)
Complete the following 2 mandatory quests:  
 1. [Fundamental: Cloud Architecture Quest](https://google.qwiklabs.com/quests/24) (10 labs) (estimated time: 11 hours)  
 2. [Introductory: Deploying Applications](https://google.qwiklabs.com/quests/26) (10 labs) (estimated time: 9 hours)  

Completion of the following optional quests are highly recommended:  
 3. [Fundamental: Security & Identity Fundamentals](https://google.qwiklabs.com/quests/40) (10 labs) (estimated time: 
    8 hours)  
 4. [Fundamental: Stackdriver](https://google.qwiklabs.com/quests/35) (10 labs) (estimated time: 9 hours)  
 5. [Fundamental: Networking in the Google Cloud](https://google.qwiklabs.com/quests/31) (7 labs) (estimated time: 9 
    hours)  
 6. [Advanced: Kubernetes in the Google Cloud](https://google.qwiklabs.com/quests/29) (10 labs) (estimated time: 10.5 
    hours)  
 7. [Advanced: Network Performance and Optimization](https://google.qwiklabs.com/quests/46) (7 labs) (estimated time: 
    6.5 hours)  
 8. Advanced: Deployment Manager (10 labs) (estimated time: 10 hours) // missing link, lab has been depracated, need to
    find equivalent    
 9. [Advanced: Managing Cloud Infrastructure with Terraform](https://google.qwiklabs.com/quests/44) (9 labs) (estimated
    time: 8.5 hours)  
 10. [Expert: Kubernetes Solutions](https://google.qwiklabs.com/quests/45) (10 labs) (estimated time: 9 hours)  
 11. [Expert: Google Cloud Solutions I: Scaling Your Infrastructure](https://google.qwiklabs.com/quests/36) (10 labs)
     (estimated time: 9.5 hours)    
     
Recommendation is to complete at least 20 of above self-paced labs before taking the exam.  

Practice and develop your coding skills with [Google Developer Codelabs](https://codelabs.developers.google.com/?cat=cloud) 
(optional) (estimated time: 4-8 hours)  

